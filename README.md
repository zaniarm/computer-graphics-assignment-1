﻿# MC-C3100 Computer Graphics, Fall 2018
# Lehtinen / Aarnio, Kemppinen, Ollikainen
#
# Assignment 1: Introduction

Student name: Zaniar Mohammadi
Student number: 659671
Hours spent on requirements (approx.): 2
Hours spent on extra credit (approx.): 2

# First, some questions about where you come from and how you got started.
# Your answers in this section will be used to improve the course.
# They will not be judged or affect your points, but please answer all of them.
# Keep it short; answering shouldn't take you more than 5-10 minutes.

- What are you studying here at Aalto? (Department, major, minor...?)
Computer Science, Big Data and Large-scale Computing

- Which year of your studies is this?
2nd year of Master studies

- Is this a mandatory course for you?
No

- Have you had something to do with graphics before? Other studies, personal interests, work?
I've done some game development but other than that, not really.

- Do you remember basic linear algebra? Matrix and vector multiplication, cross product, that sort of thing?
I do remember basic linear algebra but a little refreshing on determinants and eigenvalues wouldn't hurt.

- How is your overall programming experience? What language are you most comfortable with?
I'm most comfortable with Java, Python, PHP.

- Do you have some experience with these things? (If not, do you have experience with something similar such as C or Direct3D?)
C++: I've had courses on C++ but that's it really
C++11: Same as above. I believe that we actually used C++11 in the course
OpenGL: Not really but I've always been interested. Some game development frameworks that I've used had some OpenGL stuff.

- Have you used a version control system such as Git, Mercurial or Subversion? Which ones?
Git mostly but I've also used subversion

- Did you go to the technology lecture?
Yes

- Did you go to exercise sessions?
Yes

- Did you work on the assignment using Aalto computers, your own computers, or both?
My own computer only

# Which parts of the assignment did you complete? Mark them 'done'.
# You can also mark non-completed parts as 'attempted' if you spent a fair amount of
# effort on them. If you do, explain the work you did in the problems/bugs section
# and leave your 'attempt' code in place (commented out if necessary) so we can see it.

(Try to get everything done! Based on previous data, virtually everyone who put in the work and did well in the first two assignments ended up finishing the course, and also reported a high level of satisfaction at the end of the course.)

                            opened this file (0p): done
                         R1 Moving an object (1p): done
R2 Generating a simple cone mesh and normals (3p): done
  R3 Converting mesh data for OpenGL viewing (3p): done
           R4 Loading a large mesh from file (3p): done

# Did you do any extra credit work?

(Describe what you did and, if there was a substantial amount of work involved, how you did it. Also describe how to use/activate your extra features, if they are interactive.)

Version control(1p): done
Rotate and scale transforms (1p): done
Better Camera (up to 3p): attempted (no trackball)
Animation (0.5p): done

Bitbucket: https://bitbucket.org/zaniarm/computer-graphics-assignment-1/
Rotating and Scaling can be done with the WASD keys. W and S scales while A and D rotates.
I've Added Camera movement so that you can rotate "left" and "right" with mouse movement by clicking and holding the left mouse button (no track ball).
The animation is working and can be toggled with the R key. Although it's working I believe there's a more optimal way of implementing it.

# Are there any known problems/bugs remaining in your code?
(Please provide a list of the problems. If possible, describe what you think the cause is, how you have attempted to diagnose or fix the problem, and how you would attempt to diagnose or fix it if you had more time or motivation. This is important: we are more likely to assign partial credit if you help us understand what's going on.)

I tested to run the code in Release mode. It worked but for some reason I couldn't load any of the assets without crashing. "Caught GL error 0x0505 (GL_OUT_OF_MEMORY)"
Everything does work in Debug mode however.

# Did you collaborate with anyone in the class?
I've helped out student Hai Phan but other than that I haven't helped anyone or received any help. Of course I watched some material on the web for the different transform matrices etc.

(Did you help others? Did others help you? Let us know who you talked to, and what sort of help you gave or received.)

# Any other comments you'd like to share about the assignment or the course so far?

(Was the assignment too long? Too hard? Fun or boring? Did you learn something, or was it a total waste of time? Can we do something differently to help you learn? Please be brutally honest; we won't take it personally.)
The assignment was really fun to do. It was difficult at first to wrap my head around the framework that we we're using and how the code actually worked. Other that that, great stuff.
